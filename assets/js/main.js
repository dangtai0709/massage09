// Init data
const users = [
  {
    image: "assets/img/demo/2np.jpg",
    link: "http://wa.me/+966591476604",
  },
  {
    image: "assets/img/demo/4np.jpg",
    link: "http://wa.me/+966564247481",
  },
  {
    image: "assets/img/demo/17.jpg",
    link: "http://wa.me/+966591476604",
  },
  {
    image: "assets/img/demo/16.jpg",
    link: "http://wa.me/+966591476604",
  },
  {
    image: "assets/img/demo/an6.jpg",
    link: "http://wa.me/+966564247481",
  },
  {
    image: "assets/img/demo/hl1.jpg",
    link: "http://wa.me/+966591476604",
  },
  {
    image: "assets/img/demo/npp1.jpg",
    link: "http://wa.me/+966591476604",
  },
  {
    image: "assets/img/demo/thanh tan.jpg",
    link: "http://wa.me/+966564247481",
  },
  {
    image: "assets/img/demo/n23.jpg",
    link: "http://wa.me/+966591476604",
  },
  {
    image: "assets/img/demo/nn2.jpg",
    link: "http://wa.me/+966591476604",
  },
  {
    image: "assets/img/demo/n15.jpg",
    link: "http://wa.me/+966564247481",
  },
  {
    image: "assets/img/demo/l3.jpg",
    link: "http://wa.me/+966591476604",
  },
];
// Render list
users.forEach(({ image, link }) => {
  const template = `
     <a href="${link}" target="_blank">
              <div class="col-sm-6 col-md-3 p0">
                <div class="box-two proerty-item">
                  <div class="item-thumb">
                    <img src="${image}" alt="" />
                  </div>
                  <div class="item-entry overflow">
                    <h5 class="text-danger text-center">Chat Now</h5>
                  </div>
                </div>
              </div>
    </a>
    `;
  document.getElementById("userList").innerHTML += template;
});
